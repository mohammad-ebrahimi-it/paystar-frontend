
export const userStatus = {
    CREATED: 1,
    PENDING: 2,
    RESEND: 3,
    REJECT: 4,
    ACTIVE: 5,
    BLOCK: 6,
}

export const status_name =  (item) => {
    switch (item) {
        case userStatus.CREATED:
            return 'ثبت نام';
        case userStatus.PENDING:
            return 'در انتظار';
        case userStatus.RESEND:
            return 'ارسال مجدد';
        case userStatus.REJECT:
            return 'رد مدارک';
        case userStatus.ACTIVE:
            return 'تایید شد';
        case userStatus.BLOCK:
            return 'مسدود شده';
    }
}