import Vue from "vue";
import VueRouter from "vue-router";


import Dashboard from "@/components/Dashboard";
import Login from "@/components/auth/Login";
import Register from "@/components/auth/Register";
import Basket from "@/components/Basket";
import Callback from "@/components/Callback";
import AddCard from "@/components/AddCard";
import Layout from "@/views/layout.vue";
import Transactions from "@/views/Transactions/Transactions.vue";
import Panel from "@/views/Panel/Panel.vue";
import store from "@/store";
import GetAllUsers from "@/views/users/GetAllUsers.vue";
import Base from "@/components/auth/Base.vue";

Vue.use(VueRouter);

const routes = [
    {
        name: "dashboard",
        path: "/",
        component: Dashboard,
        children: [],
        meta: {
            permission: null,
            isAuth: false
        }

    },
    {
        name: "base-auth",
        path: "/",
        component: Base,
        children: [

            {
                name: "login",
                path: "/login",
                component: Login,
                meta: {
                    permission: null,
                    isAuth: false
                }
            },
            {
                name: "register",
                path: "/register",
                component: Register,
                meta: {
                    permission: null,
                    isAuth: false
                }
            }
        ]
    },
    {
        name: "basket",
        path: "/basket",
        component: Basket,
        meta: {
            permission: null,
            isAuth: false
        }
    },
    {
        name: "callback",
        path: "/callback",
        component: Callback,
        meta: {
            permission: null,
            isAuth: false
        }
    },
    {
        path: "/panel",
        component: Layout,
        children: [
            {
                name: "basePanel",
                path: "/",
                component: Panel,
                meta: {
                    permission: null,
                    isAuth: true
                }

            },
            {
                name: "transaction",
                path: "/transaction",
                component: Transactions,
                meta: {
                    permission: null,
                    isAuth: true
                }
            },
            {
                name: "admin.users",
                path: "/admin/users",
                component: GetAllUsers,
                meta: {
                    permission: "user.list",
                    isAuth: true
                }
            },

            {
                name: "cards",
                path: "/cards",
                component: AddCard,
                meta: {
                    permission: null,
                    isAuth: true
                }
                // beforeEnter: (to, from, next) => {
                //     if(ability.can(to.meta.permission)) {
                //         next()
                //     } else {
                //         next('/panel')
                //     }
                // }
            }
        ]
    }
];


const router = new VueRouter({
    routes, mode: "history"
});

router.beforeEach(async (to, from, next) => {

    if (to.matched.some(record => record.meta.isAuth)) {
       setTimeout(()=> {
           if (!store.getters.isAuthenticate) {
               next({
                   name: "login",
                   query: { redirect: to.fullPath }
               });
           } else {
               next();
           }
       }, 200)

    } else {
        next();
    }
});

export default router;
