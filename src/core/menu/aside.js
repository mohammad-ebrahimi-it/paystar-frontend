export const menu = [
    {
        title: "داشبورد",
        icon: "mdi-home-city",
        route: "basePanel",
        permission: null,
    },
    {
        title: "تراکنش ها",
        icon: "mdi-cash-sync",
        route: "transaction",
        permission: null

    },
    {
        title: "لیست خریدها ",
        icon: "mdi-playlist-check",
        route: null,
        permission: null

    },
    {
        title: "کارت ها",
        icon: "mdi-account-credit-card",
        route: "cards",
        permission: null

    },
    {
        title: "پیگیری تراکنش",
        icon: "mdi-account-credit-card",
        route: null,
        permission: null
    },
    {
        title: "تیکت",
        icon: "mdi-face-agent",
        route: null,
        permission: null
    },
    {
        title: "کاربران",
        icon: "mdi-account",
        route: 'admin.users',
        permission: 'user_list'
    }
];

