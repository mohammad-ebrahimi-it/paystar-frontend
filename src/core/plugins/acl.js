import {abilitiesPlugin} from '@casl/vue'
import ability from "@/core/plugins/ability";

//Check if active deactivate show main part replace $can
export function can(permission, type){
    return window.Vue.$can('all', permission);
}

function directiveCan(el, binding) {
    return !can(binding.value);
}

function install(Vue){
    Vue.prototype.can = can
    Vue.use(abilitiesPlugin, ability)

    Vue.directive('can', {
        bind: directiveCan,
        update : directiveCan
    })
}

export default {
    install,
    can
}