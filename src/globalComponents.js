import acl from "@/core/plugins/acl";


const GlobalComponents = {
    install(Vue) {
        Vue.use(acl)
    }
}

export default GlobalComponents