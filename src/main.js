import Vue from "vue";
import "./plugins/axios";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import router from "@/router";
import mixins from "./mixin";
import store from "./store";
import VueToast from "vue-toast-notification";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import GlobalComponents from "./globalComponents";
import { can } from "@/core/plugins/acl";
import i18n from './i18n'
Vue.use(GlobalComponents);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

Vue.use(VueToast, {
    rtl: true
});
Vue.use(mixins);

Vue.config.productionTip = false;



window.Vue = new Vue({
    methods: {

    },
    async beforeMount() {
        await this.getHeaders();
        let token = localStorage.getItem("token");
        if (token) {
            await this.getAbility();
            this.$forceUpdate();
        }

    },
    vuetify,
    router,
    store,
    i18n,
    render: (h) => h(App)
}).$mount("#app");
