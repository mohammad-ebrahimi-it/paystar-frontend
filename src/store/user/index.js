const state = {
    user: {}
};
const getters = {
    user(state) {
        return state.user;
    }
};

const actions = {
    setUser(context, user) {
        context.commit('user', user)
    }
};

const mutations = {
    user(state, user) {
        state.user = user
    }
}
export default {
    state,
    actions,
    mutations,
    getters
}