const server = process.env.VUE_APP_BACKEND_URL;
import Vue from 'vue'

const configs = {
    headers: {
        Accept: 'application/json',
        "Access-Control-Allow-Origin": "*",
        'Content-Type': 'application/json',
    },
    withCredentials: true,
};
const state = {
    count: 0,
    baskets: [],
};

const getters = {
    baskets(state) {
        return state.baskets;
    },
     count(state) {
        return state.count;
     }
};

const actions = {
    addBaskets(context, baskets) {
        axios.post(server+'/basket/add/'+baskets.id,{}, configs)
            .then((({data}) => {
                Vue.$toast.success('به سبد خرید اضافه شد.');
                context.dispatch("getBaskets")
            })).catch(({response})=> {
                Vue.$toast.error('مشکلی در سمت سرور پیش آمده است')
        })
    },

    getBaskets(context) {
        axios.get(server+'/basket', configs)
            .then((({data}) => {
                context.commit('baskets', data.data)
                context.commit('count', data.data.length)
            }))
    }

};

const mutations = {
    count(state, count) {
        return state.count = count;
    },
    baskets(state,  baskets) {
        return state.baskets = baskets;
    }
};
export default {
    state,
    getters,
    actions,
    mutations
}