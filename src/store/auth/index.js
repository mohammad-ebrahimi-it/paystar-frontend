const state = {
    token: null,
    isAuthenticate: false
};
const getters = {
    token(state) {
        return state.token;
    },
    isAuthenticate(state) {
        return state.isAuthenticate;
    }
};

const actions = {
    setToken(context, token) {
        localStorage.setItem('token', token)
        context.commit('token', token)
    },
    setIsAuthenticate(context, status) {
        context.commit('isAuthenticate', status)
    },
    hasToken(context) {
        context.commit('token', localStorage.getItem('token'))
    }

};

const mutations = {
    token(state, token) {
        return state.token = token;
    },
    isAuthenticate(state, status) {
        return state.isAuthenticate = status;
    }

}
export default {
    state,
    actions,
    mutations,
    getters
}
