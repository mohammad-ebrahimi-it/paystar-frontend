import Vue from 'vue'
import Vuex from 'vuex'
import user from "@/store/user";
import auth from "@/store/auth"
import basket from "@/store/basket"
import layout from "@/store/layout"
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    auth,
    basket,
    layout,
  }
})
