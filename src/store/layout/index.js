const state = {
    breadcrumb: []
};

const getters = {
    getBreadcrumb(state) {
        return state.breadcrumb;
    }
};

const mutations = {
    setBreadcrumb(state, payload) {
        state.breadcrumb = payload;
    }
};

const actions = {
    setBreadcrumb(context, payload) {
        context.commit('setBreadcrumb', payload)
    }
};


export default {
    state,
    actions,
    getters,
    mutations
}