import { mapGetters } from "vuex";

const server = process.env.VUE_APP_BACKEND_URL;
const source = process.env.VUE_APP_SOURCE_URL;

const Mixin = {
    methods: {
        getFilePath(path) {
            return source + path;
        },
        async checkToken() {
            await this.$store.dispatch("hasToken", "");
            this.getApi("/user").then(({ data }) => {
                this.$store.dispatch("setToken", localStorage.getItem("token"));
                this.$store.dispatch("setUser", data.data);
                this.$store.dispatch("setIsAuthenticate", true);
            }).catch(({}) => {
                this.$store.dispatch("setToken", "");
                this.$store.dispatch("setIsAuthenticate", false);
                // this.$router.push({"name": "login"})
                localStorage.removeItem("token");
            });
        },
        postApi(url = "/", data = {}) {
            return axios.post(server + url, data, {
                headers: this.getHeaders(),
                withCredentials: true
            });
        },
        getApi(url = "/", data = {}) {
            return axios.get(server + url, {
                headers: this.getHeaders(),
                withCredentials: true,
                params: data,
            });
        },
        getHeaders() {
            return {
                Authorization: `Bearer ${this.token}`,
                Accept: "application/json",
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"

            };
        },
        logout() {
            this.$store.dispatch("setIsAuthenticate", false);
            localStorage.removeItem("token");
            this.$router.push({ "name": "dashboard" });
            this.$toast.success("شما از سیستم خارج شدید!");
        },
        // blank(val) {
        //     if (typeof val === 'undefined')
        //         return false;
        //
        //     if (typeof val === 'object')
        //         return (Object.keys(val).length === 0
        //               || val.length === 0);
        //
        //     if (typeof val === 'string')
        //         return ( val === ''
        //               || val === null
        //               || val === 'null'
        //               || val.trim() === ''
        //               || val.length === 0 );
        //     return false;
        // },
        // filled(val) {
        //     return !this.blank(val);
        // },
        getAbility() {
            this.getApi("/access").then(({ data }) => {
                let permission = [];
                // console.log(data);
                data.data.map(item => {
                    item.permission.map(value => {
                        permission.push({action: value.slug, subject: 'all'});
                    });
                });
                this.$ability.update(permission);
            });
        }

    },
    computed: {
        ...mapGetters(["user", "isAuthenticate", "token", "count", "baskets"])
    }
};

export { Mixin };
export default {
    install(Vue, option) {
        Vue.mixin(Mixin);
    }
};
